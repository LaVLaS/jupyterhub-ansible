<p align="center">
    <img src="datahub_logo.png" width="400" height="105" alt="Data Hub, an AI platform powered by Open Source" title="Data Hub, an AI platform powered by Open Source" />
</p>

JupyterHub APB
=========

[![Docker Repository on Quay](https://quay.io/repository/opendatahub/jupyterhub-apb/status "Docker Repository on Quay")](https://quay.io/repository/opendatahub/jupyterhub-apb)

This APB allows you to install JupyterHub with Spark Operator which are both core part of OpenDataHub.io.

Components
=========

List of componets follows:

* Jupyterhub
* PostgreSQL
* Spark Operator (optional, on by default)
* Ceph (S3 replacement for development)

JupyterHub Singleuser Server images:

* Minimal
* Scipy
* Tensorflow
* Spark Minimal (spawns Ephemeral Spark cluster)
* Spark Scipy (spawns Ephemeral Spark cluster)

How to test
=========

You can deploy this APB from our image on Quay.io - [quay.io/opendatahub/jupyterhub-apb](https://quay.io/repository/opendatahub/jupyterhub-apb) by using the [APB CLI](https://github.com/ansibleplaybookbundle/ansible-playbook-bundle/blob/master/docs/apb_cli.md) or [Ansible Service Broker](https://github.com/openshift/ansible-service-broker#getting-started-with-the-ansible-service-broker).

There are 2 plans ready for you:

* prod
* dev

The main difference is that `dev` plan will deploy [Ceph](https://github.com/ceph/ceph-container) for S3 endpoint, so that you don't have to worry about setting up Ceph Object Storage or pay for AWS S3 while developing and testing things locally. The `dev` plan also deploys only one JupyterHub Singleuser Server Image (Spark Minimal) to speed up the setup time and save resources. You can also turn of Spark Operator while deploying the `dev` plan - also to save resources.

See [apb.yml](apb.yml) for the list of parameters.


How to Install and Use APB cli
==============================

1. First step is to install the apb tool

   ##### On Linux

   ```
   dnf -y install dnf-plugins-core
   dnf -y copr enable @ansible-service-broker/ansible-service-broker-latest
   dnf -y install apb
   ``` 
   ##### On Mac

   Download apb darwin prebuilt binary from latest [apb releases](https://github.com/automationbroker/apb/releases/tag/apb-2.0.2-1).   
   Place the downloaded binary in a folder included in your $PATH.   
 
2. Next you will need to add the Open Data Hub registry

   ```
   apb registry add opendatahub --type quay --org opendatahub
   ```

   Now you can check the list of imported APBs

   ```
   apb bundle list
   ```

3. Install and provision the jupyterhub-apb bundle. 

   Make sure you are logged in to OpenShift cluster you want to deploy to and that you are in the project you want to deploy to. You can also add `--namespace` to the following command to deploy to different namespace. To deploy JupyterHub, run 

   ```
   apb bundle provision jupyterhub-apb -s admin -f
   ```

   The `-s` parameter specifies which role to use during deployment. JupyterHub APB needs `admin` role because it creates role bindings. The parameter `-f` makes sure the logs from deplyoment are forwarded to your terminal.

Deploying images from registry
=========

By default JupyterHub APB will deploy OpenShift ImageStreams and BuildConfigs so that images are built in OpenShift. If you want to use pre-built images (e.g. your cluster is offline), specify `registry` and `repository` parameters during bundle provisioning. We provide all the needed images in registry `quay.io` and repository `odh-jupyterhub`.


Ceph Storage
=========
Openshift permissions for deploying Ceph
---------
Openshift user requires a security context to deploy the Ceph container. Failure to do so will cause the ceph pod initialization to fail with the message "mkdir: cannot create directory '/var/lib/ceph': Permission denied""
Prior to provision the apb bundle run this command:

```
oc --as system:admin adm policy add-scc-to-user anyuid system:serviceaccount:myproject:default
```

Accessing Ceph storage from the host machine using AWS cli
---------
If you want to create a S3 bucket or upload any data from the host machine, you can use the [AWS cli](https://aws.amazon.com/cli).
In order to make the ceph pod accessible from the host, you'll need to forward a port from the host to the ceph container.

Once the pod has been created and running successfully, run the command below to forward a port to the ceph container
```
# Instruct openshift to forward a localhost port to the ceph container
oc port-forward ceph-nano-0 8000 &

# Default S3 access key: foo
# Default S3 secrete key: bar
# Now you can specify an endpoint url as the loopback address listening on the forwarded port
aws --endpoint-url http://127.0.0.1:8000 s3 mb s3://${S3_BUCKET_NAME}
aws --endpoint-url http://127.0.0.1:8000 s3 cp ${DATA_FILENAME} s3://${S3_BUCKET_NAME}
aws --endpoint-url http://127.0.0.1:8000 s3 ls s3://${S3_BUCKET_NAME}
```

