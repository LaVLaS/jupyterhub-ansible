# Contributing to the project

## How to make and submit changes to the APB

1. Fork the repository
2. Clone your fork
3. Add Git remote for the upstream repository
    ```
    git remote add upstream https://gitlab.com/opendatahub/jupyterhub-ansible
    ```
4. Create a Quay.io account
5. Create a new repository `jupyterhub-apb` and configure automatic build trigger:
    * Make sure you mark it as `Public`
    * Select **Link to a GitLab Repository Push**
    * When prompted to "Select organization", select your Gitlab account
    * Select your `jupyterhub-ansible` fork
    * Leave **Trigger for all branches and tags** selected
    * Select the Dockerfile (`/Dockerfile`)
    * Select the context (`/`)
    * Skip the Robot Account configuration by clicking Continue

Now you have everything configured to start making changes

1. Create a branch in your cloned repository
    ```
    git checkout -b my-first-change
    ```
2. Make the change to the APB source
3. Commit the changes
    ```
    git commit -a -m "Updated APB"
    ```
4. Push your changes to your forked repository
    ```
    git push --set-upstream origin my-first-change
    ```
5. Go to your Quay.io repository, to the **Builds** tab - you should see a new build running.

To test your changes - i.e. to deploy updated version of the APB to OpenShift, follow the (README.md and install APB cli)[https://gitlab.com/opendatahub/jupyterhub-ansible#how-to-install-and-use-apb-cli].

1. Add your Quay.io repository to your APB configuration
    ```
    apb registry add opendatahub-dev --type quay --org <your-quay-io-username>
    ```
2. As the newly built image has a tag matching the branch name (e.g. `my-first-change`), you will need to update the `Tag` option for your `opendatahub-dev` registry in `~/.apb/registries.json` - i.e. in this case set `Tag: "my-first-change"`
3. Run refresh for the APB cli
    ```
    apb bundle list --refresh
    ```
4. You can deploy your update version of the APB now
    ```
    apb provision jupyterhub-apb -s admin -f -r opendatahub-dev
    ```

Once you are happy with your changes, you can submit a Merge Request