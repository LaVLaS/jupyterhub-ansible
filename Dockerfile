FROM ansibleplaybookbundle/apb-base:canary

LABEL "com.redhat.apb.spec"=\
"dmVyc2lvbjogMS4wCm5hbWU6IGp1cHl0ZXJodWItYXBiCmRlc2NyaXB0aW9uOiBPcGVuIERhdGEg\
SHViIGlzIGFuIEFJLWFzLWEtc2VydmljZSBvcGVuIHNvdXJjZSBwbGF0Zm9ybSBkZXNpZ25lZCBm\
b3IgT3BlblNoaWZ0CmJpbmRhYmxlOiBGYWxzZQphc3luYzogb3B0aW9uYWwKbWV0YWRhdGE6CiAg\
ZGlzcGxheU5hbWU6IE9wZW4gRGF0YSBIdWIgQ29yZSAoQVBCKQogIGNvbnNvbGUub3BlbnNoaWZ0\
LmlvL2ljb25DbGFzczogImZhIGZhLWdyYWR1YXRpb24tY2FwIgogIGxvbmdEZXNjcmlwdGlvbjog\
SW5zdGFsbHMgY29yZSBjb21wb25lbnRzIG9mIE9wZW4gRGF0YSBIdWIuCiAgcHJvdmlkZXJEaXNw\
bGF5TmFtZTogT3BlbiBEYXRhIEh1YgpwbGFuczoKLSBuYW1lOiBwcm9kCiAgZGVzY3JpcHRpb246\
IFRoaXMgcGxhbiBkZXBsb3lzIGEgcHJvZHVjdGlvbiB2ZXJzaW9uIG9mIGp1cHl0ZXJodWItYXBi\
CiAgZnJlZTogVHJ1ZQogIG1ldGFkYXRhOiB7fQogIHBhcmFtZXRlcnM6CiAgLSBuYW1lOiBkYl9t\
ZW1vcnkKICAgIHR5cGU6IGVudW0KICAgIGVudW06IFsnMkdpJywgJzFHaSddCiAgICBkZWZhdWx0\
OiAnMUdpJwogICAgcmVxdWlyZWQ6IHRydWUKICAtIG5hbWU6IGp1cHl0ZXJodWJfbWVtb3J5CiAg\
ICB0eXBlOiBlbnVtCiAgICBlbnVtOiBbJzJHaScsICcxR2knXQogICAgZGVmYXVsdDogJzFHaScK\
ICAgIHJlcXVpcmVkOiB0cnVlCiAgLSBuYW1lOiBzM19lbmRwb2ludF91cmwKICAgIHR5cGU6IHN0\
cmluZwogICAgcmVxdWlyZWQ6IHRydWUKICAtIG5hbWU6IHN0b3JhZ2VfY2xhc3MKICAgIGRlc2Ny\
aXB0aW9uOiBTdG9yYWdlIGNsYXNzIHRvIGJlIHVzZWQgZm9yIGFsbCB0aGUgUFZDcyBpbiB0aGUg\
YXBwCiAgICB0eXBlOiBzdHJpbmcKICAtIG5hbWU6IHJlZ2lzdHJ5CiAgICBkZXNjcmlwdGlvbjog\
VVJMIG9mIHJlZ2lzdHJ5IHRvIHB1bGwgaW1hZ2VzIGZyb20gKG9wdGlvbmFsOyBpbWFnZXMgd2ls\
bCBiZSBidWlsdCBpZiBub3QgcHJvdmlkZWQpCiAgICB0eXBlOiBzdHJpbmcKICAtIG5hbWU6IHJl\
cG9zaXRvcnkKICAgIGRlc2NyaXB0aW9uOiBOYW1lIG9mIHRoZSBpbWFnZSByZXBvc2l0b3J5IGlu\
IHRoZSByZWdpc3RyeSAobWFuZGF0b3J5IGlmIHJlZ2lzdHJ5IGlzIHByb3ZpZGVkKQogICAgdHlw\
ZTogc3RyaW5nCi0gbmFtZTogZGV2CiAgZGVzY3JpcHRpb246IFRoaXMgcGxhbiBkZXBsb3lzIGp1\
cHl0ZXJodWItYXBiIGZvciBkZXZlbG9wbWVudCAtIHdpdGggUzMgQ2VwaAogIGZyZWU6IFRydWUK\
ICBtZXRhZGF0YToge30KICBwYXJhbWV0ZXJzOgogIC0gbmFtZTogZGJfbWVtb3J5CiAgICB0eXBl\
OiBlbnVtCiAgICBlbnVtOiBbJzJHaScsICcxR2knLCAnNTEyTWknLCAnMjU2TWknXQogICAgZGVm\
YXVsdDogJzI1Nk1pJwogICAgcmVxdWlyZWQ6IHRydWUKICAtIG5hbWU6IGp1cHl0ZXJodWJfbWVt\
b3J5CiAgICB0eXBlOiBlbnVtCiAgICBlbnVtOiBbJzJHaScsICcxR2knLCAnNTEyTWknLCAnMjU2\
TWknXQogICAgZGVmYXVsdDogJzI1Nk1pJwogICAgcmVxdWlyZWQ6IHRydWUKICAtIG5hbWU6IHMz\
X2VuZHBvaW50X3VybAogICAgdHlwZTogc3RyaW5nCiAgICBkZWZhdWx0OiAiaHR0cDovL2NlcGg6\
ODAwMCIKICAgIHJlcXVpcmVkOiB0cnVlCiAgLSBuYW1lOiBzdG9yYWdlX2NsYXNzCiAgICBkZXNj\
cmlwdGlvbjogU3RvcmFnZSBjbGFzcyB0byBiZSB1c2VkIGZvciBhbGwgdGhlIFBWQ3MgaW4gdGhl\
IGFwcAogICAgdHlwZTogc3RyaW5nCiAgLSBuYW1lOiBzcGFya19vcGVyYXRvcgogICAgZGVzY3Jp\
cHRpb246IERlcGxveSBzcGFyayBvcGVyYXRvcj8KICAgIHR5cGU6IGJvb2wKICAgIGRlZmF1bHQ6\
IFRydWUKICAtIG5hbWU6IGRlcGxveV9hbGxfbm90ZWJvb2tzCiAgICBkZXNjcmlwdGlvbjogQWRk\
IGFsbCBKdXB5dGVyIG5vdGVib29rIGltYWdlcyAocmVzb3VyY2UgaGVhdnkgd2hpbGUgYnVpbGRp\
bmcpCiAgICB0eXBlOiBib29sCiAgICBkZWZhdWx0OiBGYWxzZQogIC0gbmFtZTogcmVnaXN0cnkK\
ICAgIGRlc2NyaXB0aW9uOiBVUkwgb2YgcmVnaXN0cnkgdG8gcHVsbCBpbWFnZXMgZnJvbSAob3B0\
aW9uYWw7IGltYWdlcyB3aWxsIGJlIGJ1aWx0IGlmIG5vdCBwcm92aWRlZCkKICAgIHR5cGU6IHN0\
cmluZwogIC0gbmFtZTogcmVwb3NpdG9yeQogICAgZGVzY3JpcHRpb246IE5hbWUgb2YgdGhlIGlt\
YWdlIHJlcG9zaXRvcnkgaW4gdGhlIHJlZ2lzdHJ5IChtYW5kYXRvcnkgaWYgcmVnaXN0cnkgaXMg\
cHJvdmlkZWQpCiAgICB0eXBlOiBzdHJpbmcK"

COPY playbooks /opt/apb/actions
COPY . /opt/ansible/roles/jupyterhub-apb
RUN chmod -R g=u /opt/{ansible,apb}
USER apb
